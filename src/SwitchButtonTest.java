import lmh.pers.ui.button.SwitchButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class SwitchButtonTest {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("SwitchButton");
                Dimension minimumSize = frame.getMinimumSize();
                minimumSize.setSize(200, 120);
                frame.setLayout(new FlowLayout(FlowLayout.CENTER));
                frame.setMinimumSize(minimumSize);
                final SwitchButton switchButton = new SwitchButton();
                final JLabel label = new JLabel("关");
                switchButton.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        if (switchButton.isSelected()) {
                            label.setText("开");
                        } else {
                            label.setText("关");
                        }
                    }
                });
//            switchButton.addItemListener(e -> {
//                if (switchButton.isSelected()) {
//                    label.setText("开");
//                } else {
//                    label.setText("关");
//                }
//            });
                frame.add(switchButton);
                frame.add(label);
                frame.pack();
                frame.setVisible(true);
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            }
        });
    }
}
